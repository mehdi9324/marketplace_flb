<?php

return [

    'minimum_allowed_distance' => env('MINIMUM_ALLOWED_DISTANCE', '5'),
];
