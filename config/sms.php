<?php

return [

    'driver' => env('SMS_DRIVER', 'kavenegar'),
    'providers' => [
        'kavenegar' => [
            'api_key' => env('KAVENEGAR_API_KEY', null),
            'sender' => env('KAVENEGAR_SENDER', null),
        ],
        'melipayamak' => [
            'api_key' => env('MELIPAYAMAK_API_KEY', null),
        ],
    ],

    // for sms
    'enabled_sms'   => env('SMS_ENABLED', false),
    'ttl_sms'       => env('SMS_TTL', 120),

    // for call
    "enabled_call"  =>  env('CALL_ENABLED', false),
    "ttl_call"      =>  env('CALL_TTL', 120),

];
