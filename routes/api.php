<?php

use App\Http\Controllers\Admin\V1\AdminController;
use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\OrderController;
use App\Http\Controllers\Api\V1\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class, 'login']);
Route::post('/verify-login', [AuthController::class, 'verifyLogin']);

Route::group(['prefix' => 'admin'], function () {
    Route::post('create-seller', [AdminController::class, 'createSeller'])->middleware(['can:create seller']);
});
Route::group(['prefix' => 'user'], function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::group(['prefix' => 'product'], function () {
        Route::post('add', [ProductController::class, 'add'])->middleware(['can:add product']);
        Route::post('show', [ProductController::class, 'show']);
        Route::post('add-to-basket', [OrderController::class, 'addToBasket'])->middleware(['can:buy product']);
        Route::post('remove-basket', [OrderController::class, 'removeBasket'])->middleware(['can:buy product']);
        Route::post('payment', [OrderController::class, 'payment'])->middleware(['can:buy product']);
    });
});
