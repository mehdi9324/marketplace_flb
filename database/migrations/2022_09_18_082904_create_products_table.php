<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->string('property')->nullable(); # "color","code","size"
            $table->string('price')->default(0);
            $table->string('stock')->default(0);
            $table->string('discount')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign("category_id")->references("id")->on("categories")
                ->nullOnDelete()->cascadeOnUpdate();
            $table->foreign("vendor_id")->references("id")->on("vendors")
                ->nullOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
