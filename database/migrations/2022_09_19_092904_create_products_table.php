<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('product_id')->nullable();
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->string('price');
            $table->string('status');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")
                ->nullOnDelete()->cascadeOnUpdate();
            $table->foreign("product_id")->references("id")->on("products")
                ->nullOnDelete()->cascadeOnUpdate();
            $table->foreign("vendor_id")->references("id")->on("vendors")
                ->nullOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
