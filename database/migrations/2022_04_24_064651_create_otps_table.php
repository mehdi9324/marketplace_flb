<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otps', function (Blueprint $table) {
            $table->id('id');
            $table->morphs('otpable');
            $table->foreignId('user_id')->nullable();
            $table->string('type')->comment('MOBILE,EMAIL')->nullable();
            $table->string('otp')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->integer('trial_count')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->index("type");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otpies');
    }
};
