<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # clear cache of permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $arr = [
            [
                'id' => 1,
                'group_id' => 1,
                'fa_name' => 'لیست کاربرها',
                'name' => 'user index'
            ],
            [
                'id' => 2,
                'group_id' => 1,
                'fa_name' => 'ثبت فروشنده جدید',
                'name' => 'create seller'
            ],
            [
                'id' => 3,
                'group_id' => 1,
                'fa_name' => 'ثبت محصول',
                'name' => 'add product'
            ],
            [
                'id' => 4,
                'group_id' => 1,
                'fa_name' => 'خرید محصول',
                'name' => 'buy product'
            ],

        ];

        foreach ($arr as $item) {
            Permission::firstOrCreate([
                'name' => $item['name'],
            ], [
                'id' => $item['id'],
                'group_id' => $item['group_id'],
                'fa_name' => $item['fa_name'],
                'name' => $item['name'],
                'is_active' => true,
            ]);
        }
    }
}
