<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rolesArray = [
            'super_admin',
            'admin',
            'seller',
            'user',
        ];

        foreach ($rolesArray as $roleName) {
            Role::query()->firstOrCreate([
                'name' => $roleName
            ]);
        }
    }
}
