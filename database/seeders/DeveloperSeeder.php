<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DeveloperSeeder extends Seeder
{
    public function run()
    {
        $role = Role::where('name', 'super_admin')->first();

        $superAdmins = User::where('type', 'super_admin')->get();
        if ($superAdmins->isNotEmpty()) {
            foreach ($superAdmins as $superAdmin) {
                $superAdmin->assignRole('super_admin');
            }

            $permission = Permission::pluck('id')->toArray();
            $role->givePermissionTo($permission);
        }
    }
}
