<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        $pass = 'Password@123456';
        $admins = [
            [
                'id' => 1,
                'first_name' => 'mehdi',
                'last_name' => 'hoseini',
                'type' => 'super_admin',
                'email' => 'mehdi.hoseini86@gmail.com',
                'email_verified_at' => now(),
                'password' => Hash::make($pass)
            ],
            [
                'id' => 2,
                'first_name' => 'reza',
                'last_name' => 'tabari',
                'type' => 'admin',
                'email' => 'reza.tabari@gmail.com',
                'email_verified_at' => now(),
                'password' => Hash::make($pass)
            ],
            [
                'id' => 3,
                'first_name' => 'hasan',
                'last_name' => 'ghafori',
                'type' => 'seller',
                'email' => 'hasan.ghafori@gmail.com',
                'email_verified_at' => now(),
                'password' => Hash::make($pass)
            ],
        ];

        foreach ($admins as $admin) {
            User::firstOrCreate(
                [
                    'id' => $admin['id']
                ],
                [
                    'first_name' => $admin['first_name'],
                    'last_name' => $admin['last_name'],
                    'type' => $admin['type'],
                    'email' => $admin['email'],
                    'password' => $admin['password'],
                ],
                $admin
            );
        }
    }
}
