<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    public function run()
    {

        $categories = [
            [
                'id' => 1,
                'name' => 'mobile',
                'is_active' => true,
            ],
            [
                'id' => 2,
                'name' => 'perfume',
                'is_active' => true,
            ],
        ];

        foreach ($categories as $category) {
            Category::firstOrCreate(
                [
                    'id' => $category['id']
                ],
                [
                    'name' => $category['name'],
                    'is_active' => $category['is_active'],
                ],
                $category
            );
        }
    }
}
