<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Otp extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];
    public const TYPE_MOBILE = 'MOBILE';
    public const TYPE_EMAIL = 'EMAIL';

    public const OTP_TEMPLATE = 'OTP';
    public const LOG_IN_WITH_WRONG_PASSWORD = 'LOG_IN_WITH_WRONG_PASSWORD';
    public const GENERATE_FORGET_PASSWORD = 'FORGET_PASSWORD';
    public const GENERATE_VERIFY_SIGN_UP = 'GENERATE_VERIFY_SIGN_UP';
    public const GENERATE_VERIFY_EMAIL = 'GENERATE_VERIFY_EMAIL';
    public const GENERATE_VERIFY_SIGN_IN = 'GENERATE_VERIFY_SIGN_IN';
    public const GENERATE_VERIFY_RESEND_OTP = 'GENERATE_VERIFY_RESEND_OTP';
    public const GENERATE_VERIFY_PASSWORD_CHANGE = 'VERIFY_PASSWORD_CHANGE';
    public const COUNT_OF_OTP_DIGITS = 5;


    /**
     * @return MorphTo
     */
    public function otpable(): MorphTo
    {
        return $this->morphTo();
    }
}
