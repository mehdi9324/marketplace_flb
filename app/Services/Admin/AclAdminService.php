<?php

namespace App\Services\Admin;

use App\Repositories\Admin\PermissionAdminRepository;
use App\Repositories\Admin\RoleAdminRepository;
use App\Services\BaseService;

class AclAdminService extends BaseService implements AclAdminServiceInterface
{
    public function __construct(
        protected PermissionAdminRepository $permissionAdminRepository,
        protected RoleAdminRepository $roleAdminRepository
    ) {
    }

    public function allPermissions()
    {
        return $this->permissionAdminRepository->allPermissions();
    }

    public function createNewRole($name, $permissions)
    {
        $role = $this->roleAdminRepository->create([
            'name' => $name
        ]);

        $role->syncPermissions($permissions);

        return true;
    }

    public function allRoles()
    {
        return $this->roleAdminRepository->all();
    }

    public function getRole($roleId)
    {
        return $this->roleAdminRepository->find($roleId);
    }

    public function updateRole($permissions, $id)
    {
        $role = $this->roleAdminRepository->find($id);

        $role->syncPermissions($permissions);

        return true;
    }
}
