<?php

namespace App\Services\Admin;

interface AclAdminServiceInterface
{
    public function allPermissions();

    public function createNewRole($name, $permissions);

    public function updateRole($permissions, $id);

    public function allRoles();

    public function getRole($roleId);
}
