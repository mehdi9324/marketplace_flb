<?php

namespace App\Services\Admin;

interface AdminServiceInterface
{
    public function create($data);
}
