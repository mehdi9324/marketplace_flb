<?php

namespace App\Services\Admin;

use App\Repositories\Admin\UserRepository;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Builder;

class AdminService extends BaseService implements AdminServiceInterface
{
    public function __construct(protected UserRepository $userRepository)
    {

    }

    public function create($data)
    {
        return $this->userRepository->create($data);
    }

}
