<?php

namespace App\Services\Api;

use App\Repositories\Admin\UserRepository;
use App\Repositories\Api\ProductRepository;
use App\Repositories\Api\VendorRepository;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Builder;

class ProductService extends BaseService implements ProductServiceInterface
{
    public function __construct(protected ProductRepository $productRepository)
    {
    }
    public function show($data,$nearVendorIds)
    {
        $data['vendor_ids'] = $nearVendorIds;
        return $this->productRepository->show($data);
    }

    public function create($data)
    {
        $this->productRepository->create($data);
    }
}
