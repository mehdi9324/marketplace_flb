<?php

namespace App\Services\Api;

use App\Services\BaseService;
use Illuminate\Support\Facades\Cache;

class CacheSystemService extends BaseService implements CacheSystemServiceInterface
{
    /**
     * @param $userID
     * @param $expireTime
     * @param string $text
     * @return mixed
     */
    public function CacheGenerateCode($userID, $expireTime, $text = 'generateCode'): mixed
    {
        return Cache::remember("$text:id:$userID", now()->addSeconds($expireTime), function () use ($userID) {
            return $userID;
        });
    }

    /**
     * @param $key
     * @param $value
     * @param $expireTime
     * @return bool
     */
    public function put($key, $value, $expireTime): bool
    {
        return Cache::put($key, $value, $expireTime);
    }

    /**
     * @param $key
     * @param $data
     * @param $expireTime
     */
    public function cacheArray($key, $data, $expireTime)
    {
        Cache::remember($key, $expireTime, function () use ($data) {
            return $data;
        });
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function getCache($key)
    {
        if (Cache::has($key)) {
            return Cache::get($key);
        }

        return null;
    }

    /**
     * @param $key
     */
    public function clearCache($key)
    {
        if (Cache::has($key)) {
            return Cache::forget($key);
        }

        return null;
    }
}
