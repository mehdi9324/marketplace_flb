<?php

namespace App\Services\Api;

interface ProductServiceInterface
{
    public function show($data,$nearVendorIds);
    public function create($data);
}
