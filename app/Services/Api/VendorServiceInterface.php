<?php

namespace App\Services\Api;

interface VendorServiceInterface
{
    public function create($data);
    public function findNearbyByLocation($latLong);

}
