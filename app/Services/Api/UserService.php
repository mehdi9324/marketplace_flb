<?php

namespace App\Services\Api;

use App\Repositories\Admin\UserRepository;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Builder;

class UserService extends BaseService implements UserServiceInterface
{
    public function __construct(protected UserRepository $adminRepository)
    {
    }


    public function findByColumn($column, $data)
    {
        return $this->adminRepository->findByColumn($column, $data);
    }

    public function create($data)
    {
        return $this->adminRepository->create($data);
    }

    public function update($data, $id)
    {
        return $this->adminRepository->update($data, $id);
    }

    public function all()
    {
        return $this->adminRepository->all();
    }
}
