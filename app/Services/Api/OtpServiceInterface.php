<?php

namespace App\Services\Api;

use App\Models\Otp;
use App\Models\User;

interface OtpServiceInterface
{
    public function generateOtp($object, $otp, $data, $type = Otp::TYPE_EMAIL, $expired_at = null): mixed;

    public function verifyOtp($userId, $otp, $morphType, $type = Otp::TYPE_EMAIL);

    public function findOpt($otp, $userId);

    public function findOtpByMorphId($id, $userId);

    public function expireOtp($otp);
}
