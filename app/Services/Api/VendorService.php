<?php

namespace App\Services\Api;

use App\Repositories\Admin\UserRepository;
use App\Repositories\Api\VendorRepository;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Builder;

class VendorService extends BaseService implements VendorServiceInterface
{
    public function __construct(protected VendorRepository $vendorRepository)
    {
    }

    public function create($data)
    {
        return $this->vendorRepository->create($data);
    }

    public function findNearbyByLocation($latLong)
    {

        $minAllowedDistance = config('market.minimum_allowed_distance');

        $explode = explode(',', $latLong);
        $lat = $explode[0];
        $long = $explode[1];
        $vendors = $this->vendorRepository->getDistance($lat, $long);
        foreach ($vendors as $vendor) {
            if ($vendor->distance < $minAllowedDistance) {
                $vendorIds[] = $vendor->id;
            }
        }
        return $vendorIds ?? null;
    }
}
