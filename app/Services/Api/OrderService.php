<?php

namespace App\Services\Api;

use App\Repositories\Admin\UserRepository;
use App\Repositories\Api\OrderRepository;
use App\Repositories\Api\ProductRepository;
use App\Repositories\Api\VendorRepository;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Builder;

class OrderService extends BaseService implements OrderServiceInterface
{
    public function __construct(protected OrderRepository $orderRepository)
    {
    }
    public function createOrder($data)
    {
        return $this->orderRepository->createOrder($data);
    }
}
