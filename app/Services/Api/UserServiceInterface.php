<?php

namespace App\Services\Api;

interface UserServiceInterface
{
    public function findByColumn($column, $data);

    public function create($data);

    public function update($data, $id);

    public function all();
}
