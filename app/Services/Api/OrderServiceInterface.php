<?php

namespace App\Services\Api;

interface OrderServiceInterface
{
    public function createOrder($data);
}
