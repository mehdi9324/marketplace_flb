<?php

namespace App\Services\Api;

use App\Models\Otp;
use App\Repositories\Admin\OtpAdminRepository;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Model;

class OtpService extends BaseService implements OtpServiceInterface
{
    public function __construct(protected OtpAdminRepository $otpAdminRepository)
    {
    }

    public function generateOtp($object, $otp, $data, $type = Otp::TYPE_MOBILE, $expired_at = null): Model
    {
        if ($expired_at == null) {
            $expired_at = now()->addSeconds(config('sms.ttl_sms'));
        }
        $userVerifyCode = [
            'user_id' => $object->id,
            'type' => $type,
            'otp' => $otp,
            'expired_at' => $expired_at,
        ];

        return $object->otp()->create($userVerifyCode);
    }

    public function verifyOtp($adminId, $otp, $morphType, $type = Otp::TYPE_MOBILE)
    {
        return  $this->otpAdminRepository->verifyOtp($adminId, $otp, $type, $morphType);
    }

    /** TODO:must be remove an replace with verifyOtp() */
    public function findOpt($otp, $adminId)
    {
        return $this->otpAdminRepository->findUserOTp($otp, $adminId);
    }

    public function findOtpByMorphId($id, $adminId)
    {
        return $this->otpAdminRepository->findOtpByMorphId($id, $adminId);
    }

    public function expireOtp($otp)
    {
        return $this->otpAdminRepository->expireOtp($otp);
    }
}
