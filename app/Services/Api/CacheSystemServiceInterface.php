<?php

namespace App\Services\Api;

use Illuminate\Support\Facades\Cache;

interface CacheSystemServiceInterface
{
    /**
     * @param $userID
     * @param $expireTime
     * @param string $text
     * @return mixed
     */
    public function CacheGenerateCode($userID, $expireTime, $text = 'generateCode');

    /**
     * @param $key
     * @param $value
     * @param $expireTime
     * @return mixed
     */
    public function put($key, $value, $expireTime);

    /**
     * @param $key
     * @param $data
     * @param $expireTime
     */
    public function cacheArray($key, $data, $expireTime);

    /**
     * @param $key
     * @return mixed|null
     */
    public function getCache($key);

    public function clearCache($key);
}
