<?php

function convertArabicStringToPersian($string)
{
    $arabic = ['ي', 'ك', 'ة'];

    $farsi = ['ی', 'ک', 'ه'];

    return str_replace($arabic, $farsi, $string);
}

function convertToEnglishNumber($string)
{
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١', '٠'];

    $num = range(0, 9);
    $convertedPersianNums = str_replace($persian, $num, $string);
    $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);

    return $englishNumbersOnly;
}

function removeExtraZero($amount)
{
    return str_contains($amount, '.') ? rtrim(rtrim($amount, '0'), '.') : $amount;
}

function createRandomInt($number)
{
    return rand(pow(10, $number - 1), pow(10, $number) - 1);
}

function is_persian($str)
{
    $chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
        'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
        'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    ];
    foreach ($chars as $char) {
        if (strpos($str, $char)) {
            return false;
        }
    }
    return true;
}

function timeToString($time)
{
    return $time ? $time->toDateTimeString() : null;
}


function generateUserCode()
{
    return rand(1000000000, 9000000000);
}
