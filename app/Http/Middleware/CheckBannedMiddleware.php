<?php

namespace App\Http\Middleware;

use App\Exceptions\ResponseCode;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class CheckBannedMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        $user = auth('api')->user();
        if($user->banned_at){
            return response()->json([
                'success' => false,
                'code' => ResponseCode::FORBIDDEN,
                'message' => __("you are banned, please contact admin"),
            ], ResponseCode::FORBIDDEN);
        }
        return $next($request);
    }
}
