<?php

namespace App\Http\Middleware;

use App\Exceptions\ResponseCode;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if (! $token = auth('api')->setRequest($request)->getToken()) {
            return response()->json([
                'message' => __('Auth Token Invalid'),
                'code' => ResponseCode::UNAUTHORIZED,
            ], 401);
        }
        auth('api')->authenticate();

        return $next($request);
    }
}
