<?php

namespace App\Http\Requests\Admin\V1;

use Illuminate\Foundation\Http\FormRequest;

class CreateSellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_name' => 'bail|required',
            'last_name' => 'bail|required',
            'email' => 'bail|required|unique:users',
            'password' => 'required|min:8|confirmed',
            'role_id' => 'required|exists:roles,id'
        ];
    }
}
