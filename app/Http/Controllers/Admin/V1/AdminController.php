<?php

namespace App\Http\Controllers\Admin\V1;

use App\Http\Controllers\Api\BaseApiController;
use App\Http\Requests\Admin\V1\CreateSellerRequest;
use App\Models\Otp;
use App\Services\Admin\AclAdminService;
use App\Services\Admin\AdminServiceInterface;
use App\Services\Api\OtpServiceInterface;
use App\Services\Api\VendorServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends BaseApiController
{
    public $otp;

    public function __construct(

        protected AdminServiceInterface $adminService,
        protected VendorServiceInterface $vendorService,
        protected AclAdminService $aclAdminService,
        protected OtpServiceInterface $otpService,
    ) {
        $this->otp = createRandomInt(Otp::COUNT_OF_OTP_DIGITS);
    }

    public function adminInfo(): JsonResponse
    {
        # return json response with collection
    }

    public function createSeller(CreateSellerRequest $createSellerRequest): JsonResponse
    {


        $this->vendorService->create([
            'name'=>$createSellerRequest['vendor']['name'],
            "code"=>$createSellerRequest['vendor']['code'],
            "lat"=>$createSellerRequest['vendor']['lat'],
            "long"=>$createSellerRequest['vendor']['long'],
            "province"=>$createSellerRequest['vendor']['province'],
            "city"=>$createSellerRequest['vendor']['city'],
            "address"=>$createSellerRequest['vendor']['address'],
            "phone"=>$createSellerRequest['vendor']['phone'],
            "admin_id"=>$createSellerRequest['vendor']['admin_id']
        ]);

        $targetRole = $this->aclAdminService->getRole($createSellerRequest->role_id);

        $seller = $this->adminService->create([
            'first_name' => $createSellerRequest->first_name,
            'last_name' => $createSellerRequest->last_name,
            'email' => $createSellerRequest->email,
            'password' => Hash::make($createSellerRequest->password),
            'active' => true
        ]);

        $seller->assignRole($targetRole->id);

        $this->vendorService->create([
            'name'=>$createSellerRequest['vendor']['name'],
            "code"=>$createSellerRequest['vendor']['code'],
            "lat"=>$createSellerRequest['vendor']['lat'],
            "long"=>$createSellerRequest['vendor']['long'],
            "province"=>$createSellerRequest['vendor']['province'],
            "city"=>$createSellerRequest['vendor']['city'],
            "address"=>$createSellerRequest['vendor']['address'],
            "phone"=>$createSellerRequest['vendor']['phone'],
            "admin_id"=>$createSellerRequest['vendor']['admin_id']
        ]);

        return $this->sendResponse([], __("seller created successfully"));
    }

    public function edit(): JsonResponse
    {
        # return json response with collection
    }

    public function changePassword(): JsonResponse
    {
        # return json response
    }

    public function index(): JsonResponse
    {
        # return json response with collection
    }

    public function loginHistory(Request $request): JsonResponse
    {
        # return json response with collection
    }
}
