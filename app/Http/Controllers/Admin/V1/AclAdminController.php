<?php

namespace App\Http\Controllers\Admin\V1;

use App\Http\Controllers\Api\BaseApiController;
use App\Http\Requests\Admin\V1\AddRoleRequest;
use App\Http\Requests\Admin\V1\EditRoleRequest;
use App\Http\Resources\Admin\V1\PermissionsCollection;
use App\Http\Resources\Admin\V1\RoleResource;
use App\Http\Resources\Admin\V1\RolesCollection;
use App\Services\Admin\AclAdminServiceInterface;
use Illuminate\Http\JsonResponse;

class AclAdminController extends BaseApiController
{
    public function __construct(
        private AclAdminServiceInterface $aclAdminService
    ) {
    }

    public function allPermissions(): JsonResponse
    {
        $permissions = $this->aclAdminService->allPermissions();

        return $this->sendResponse(new PermissionsCollection($permissions));
    }

    public function addRole(AddRoleRequest $request): JsonResponse
    {
        $this->aclAdminService->createNewRole($request->name, $request->permissions);

        return $this->sendResponse([]);
    }

    public function allRoles(): JsonResponse
    {
        $roles = $this->aclAdminService->allRoles();

        return $this->sendResponse(new RolesCollection($roles));
    }

    public function roleData($roleId): JsonResponse
    {
        $role = $this->aclAdminService->getRole($roleId);

        return $this->sendResponse(new RoleResource($role));
    }

    public function editRole(EditRoleRequest $request): JsonResponse
    {
        $this->aclAdminService->updateRole($request->permissions, $request->id);

        return $this->sendResponse([], __("role updated successfully"));
    }
}
