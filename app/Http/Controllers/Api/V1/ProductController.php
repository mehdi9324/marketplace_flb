<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\BaseApiController;
use App\Http\Requests\Api\V1\AddProductRequest;
use App\Http\Requests\Api\V1\ShowProductsRequest;
use App\Services\Api\ProductServiceInterface;
use App\Services\Api\UserServiceInterface;
use App\Services\Api\VendorServiceInterface;
use Illuminate\Http\JsonResponse;

class ProductController extends BaseApiController
{
    public function __construct(private UserServiceInterface $userService,
                                private ProductServiceInterface $productService,
                                protected VendorServiceInterface $vendorService)
    {
    }

    public function add(AddProductRequest $addProductRequest): JsonResponse
    {
        $this->productService->create([
            'name'=>$addProductRequest['name'],
            "category_id"=>$addProductRequest['category_id'],
            "vendor_id"=>$addProductRequest['vendor_id'],
            "property"=>json_encode($addProductRequest['property']),
            "price"=>$addProductRequest['price'],
        ]);

        return $this->sendResponse([], __("seller created successfully"));

    }
    public function show(ShowProductsRequest $showProductsRequest): JsonResponse
    {
        $nearVendorIds = $this->vendorService->findNearbyByLocation($showProductsRequest->latLong);

        if (is_null($nearVendorIds)){
            return $this->sendError(__('There is no vendor near you'));
        }

        $productList = $this->productService->show($showProductsRequest->all(),$nearVendorIds);
        return $this->sendResponse($productList, __("products fetched successfully"));

    }

    public function buy(): JsonResponse
    {

    }

}
