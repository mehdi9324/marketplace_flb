<?php

namespace App\Http\Controllers\Api\V1;

use App\Exceptions\ResponseCode;
use App\Http\Controllers\Api\BaseApiController;
use App\Http\Requests\Api\V1\LoginRequest;
use App\Http\Requests\Api\V1\RegisterUserRequest;
use App\Http\Requests\Api\V1\VerifyLoginOtpRequest;
use App\Models\Otp;
use App\Models\User;
use App\Notifications\SendOtpNotification;
use App\Services\Admin\AdminService;
use App\Services\Api\OtpServiceInterface;
use App\Services\Api\UserService;
use App\Services\Api\UserServiceInterface;
use App\Services\Api\CacheSystemService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseApiController
{
    protected int $otp;

    public function __construct(
        private UserServiceInterface $userService,
        private OtpServiceInterface $otpService,
        private CacheSystemService $cacheSystemService
    )
    {
        $this->otp = createRandomInt(Otp::COUNT_OF_OTP_DIGITS);
    }

    public function register(RegisterUserRequest $registerUserRequest)
    {
        $user = $this->userService->create([
            'first_name' => $registerUserRequest->first_name,
            'last_name' => $registerUserRequest->last_name,
            'email' => $registerUserRequest->email,
            'password' => Hash::make($registerUserRequest->password),
            'active' => true
        ]);

        $user->assignRole('user');

        $this->otpService->generateOtp($user, $this->otp, [], Otp::TYPE_EMAIL);

        $this->cacheSystemService->CacheGenerateCode($user->id, config('sms.ttl_sms'), Otp::GENERATE_VERIFY_EMAIL);

        $user->notify(new SendOtpNotification($this->otp));

        return $this->sendResponse([], __('user created & otp sent to your email'));

    }

    public function login(LoginRequest $request): JsonResponse
    {
        $data = $request->only(['email', 'password']);

        if (!Auth::guard('api')->attempt($data)) {
            return $this->sendError(__('the information is incorrect'));
        }

        $user = $this->userService->findByColumn('email', $request->email);

        if ($this->cacheSystemService->getCache(Otp::GENERATE_VERIFY_EMAIL . ':id:' . $user->id)) {
            return $this->sendError(__('The code was recently sent to you'));
        }

        if (!$user->active) {
            return $this->sendError(__('user is not active'));
        }
        $this->otpService->generateOtp($user, $this->otp, [], Otp::TYPE_EMAIL);

        $this->cacheSystemService->CacheGenerateCode($user->id, config('sms.ttl_sms'), Otp::GENERATE_VERIFY_EMAIL);

        $user->notify(new SendOtpNotification($this->otp));

        return $this->sendResponse([], __('otp sent to your email'));
    }

    public function verifyLogin(VerifyLoginOtpRequest $verifyLoginOtpRequest): JsonResponse
    {
        $user = $this->userService->findByColumn('email', $verifyLoginOtpRequest->email);

        if (is_null($user)) {
            return $this->sendError(__('email is wrong'), ResponseCode::NOT_FOUND);
        }

        $otp = $this->otpService->verifyOtp($user->id, $verifyLoginOtpRequest->otp, User::class, Otp::TYPE_EMAIL);

        if (is_null($otp)) {
            return $this->sendError(__('otp not found'), ResponseCode::NOT_FOUND);
        }

        $otp = $this->otpService->verifyOtp($user->id, $verifyLoginOtpRequest->otp, User::class, Otp::TYPE_EMAIL);

        $token = auth('api')->login($user);

        $this->otpService->expireOtp($otp);

        return $this->respondWithToken($token);
    }

    public function logout(): JsonResponse
    {
        auth('api')->logout();

        return $this->sendResponse([], __('Logout completed successfully'));
    }
}
