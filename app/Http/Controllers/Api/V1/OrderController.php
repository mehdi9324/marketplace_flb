<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\BaseApiController;
use App\Http\Requests\Api\V1\OrderBuyRequest;
use App\Services\Api\OrderServiceInterface;
use App\Services\Api\ProductServiceInterface;
use App\Services\Api\UserServiceInterface;
use Illuminate\Http\JsonResponse;

class OrderController extends BaseApiController
{
    public function __construct(
        private UserServiceInterface $userService,
        private ProductServiceInterface $productService,
        private OrderServiceInterface $orderService,
    )
    {
    }


    public function addToBasket(OrderBuyRequest $orderBuyRequest): JsonResponse
    {
        #todo we can save into basket_table or save in session

    }

    public function removeBasket()
    {

    }
    public function payment()
    {

    }

}
