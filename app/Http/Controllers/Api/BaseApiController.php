<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ResponseCode;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class BaseApiController extends Controller
{
    public function sendResponse($data, string $message = null, int $code = ResponseCode::OK): JsonResponse
    {
        if ($message == null) {
            $message = __('Data Received');
        }

        return response()->json([
            'success'=>true,
            'code' => $code,
            'message'=>$message,
            'data'=>$data,
        ], $code);
    }

    public function sendResponseWithPagination($data, $pagination, $message = null, $code = ResponseCode::OK): JsonResponse
    {
        if ($message == null) {
            $message = __('Data Received');
        }

        return response()->json([
            'success'=>true,
            'code' => $code,
            'message'=>$message,
            'data'=>$data,
            'pagination'=>$pagination,
        ], $code);
    }
    public function sendError($error, int $code = ResponseCode::BAD_REQUEST): JsonResponse
    {
        if ($error == null) {
            $error = __('An Error Has Occurred');
        }

        return response()->json([
            'success'=>false,
            'code' => $code,
            'message'=>$error,
        ], $code);
    }
    public function sendErrorWithData($errorMessage, $data, int $code = ResponseCode::BAD_REQUEST): JsonResponse
    {
        if ($errorMessage == null) {
            $errorMessage = __('An Error Has Occurred');
        }

        return response()->json([
            'success'=>false,
            'code' => $code,
            'message'=>$errorMessage,
            'data'=>$data,
        ], $code);
    }

    public function respondWithToken($token): JsonResponse
    {
        $data = [
            'token_type'    =>  'Bearer',
            'access_token'  =>  $token,
            'expires_in'    =>  auth('api')->factory()->getTTL() * 60,
        ];

        return $this->sendResponse($data);
    }
}
