<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {

        });

        if(config("app.debug") == True && request()->is("api/*")) {
            $this->apiProductionException();
        }
    }

    protected function apiProductionException()
    {
        $this->renderable(function (NotFoundHttpException $e, $request) {
            return $this->sendError(__("exceptions.page_not_found"), ResponseCode::NOT_FOUND);
        });

        $this->renderable(function (AccessDeniedHttpException $e, $request) {
            return $this->sendError(__("exceptions.access_denied"), ResponseCode::FORBIDDEN);
        });

        $this->renderable(function (MethodNotAllowedHttpException $e, $request) {
            return $this->sendError(__("exceptions.method_not_valid"), ResponseCode::METHOD_NOT_ALLOWED);
        });

        $this->renderable(function (ServiceUnavailableHttpException $e, $request) {
            return $this->sendError(__("exceptions.internal_server_error"), ResponseCode::INTERNAL_SERVER_ERROR);
        });

    }
    public function sendError($error, int $code = ResponseCode::BAD_REQUEST): JsonResponse
    {
        return response()->json([
            'success'=>false,
            'code' => $code,
            'message'=>$error,
        ], $code);
    }
}
