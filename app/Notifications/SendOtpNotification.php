<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendOtpNotification extends Notification
{
    use Queueable;

    private string $title;

    public function __construct(protected string $otp, protected $agent = null)
    {
        $this->queue = "notifications";
        $this->title = 'otp email from flash box';
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $data = [
            "ip" => $this->agent['ip']?? null,
            "agent" => $this->agent['device'] ?? null,
            "otp" => $this->otp,
            "time" => Carbon::now()
        ];

        return (new MailMessage())->subject($this->title)->view('mail.verify-email', $data);
    }

    public function toDatabase($notifiable)
    {
        //
    }
}
