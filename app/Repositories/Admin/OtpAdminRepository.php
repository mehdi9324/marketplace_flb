<?php

namespace App\Repositories\Admin;

use App\Models\Otp;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class OtpAdminRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Otp::class;
    }

    public function verifyOtp($adminId, $otp, $type, $morphType)
    {
        return $this->allQuery()
            ->where('user_id', $adminId)
            ->where('type', $type)
            ->where('otp', $otp)
            ->where('expired_at', '>=', Carbon::now())
            ->where('otpable_type', $morphType)
            ->first();
    }

    public function findUserOTp($otp, $adminId)
    {
        return $this->allQuery()
            ->where([['user_id', $adminId], ['otp', $otp], ['expired_at', '>', Carbon::now()]])
            ->orderBy('id', 'desc')->first();
    }

    public function findOtpByMorphId($id, $adminId)
    {
        return $this->allQuery()
            ->where([['user_id', $adminId], ['expired_at', '>', Carbon::now()], ['otpable_id', $id]])
            ->orderBy('id', 'desc')
            ->first();
    }

    public function expireOtp($otp)
    {
        return $otp->update([
            'expired_at' => Carbon::now(),
        ]);
    }
}
