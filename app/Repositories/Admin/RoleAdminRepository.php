<?php

namespace App\Repositories\Admin;

use App\Repositories\BaseRepository;
use Spatie\Permission\Models\Role;

class RoleAdminRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Role::class;
    }
}
