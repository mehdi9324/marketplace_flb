<?php

namespace App\Repositories\Admin;

use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    private $fieldSearchable = [];

    /**
     * @inheritDoc
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * @inheritDoc
     */
    public function model()
    {
        return User::class;
    }


    public function findByColumn($column, $data)
    {
        return $this->allQuery()->where($column, $data)->first();
    }
}
