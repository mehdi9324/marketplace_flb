<?php

namespace App\Repositories\Api;

use App\Models\Vendor;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class VendorRepository extends BaseRepository
{
    private $fieldSearchable = [];

    /**
     * @inheritDoc
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Vendor::class;
    }

    public function getDistance($lat, $long)
    {
        $vendors = Vendor::select("vendors.id"
            ,DB::raw("6371 * acos(cos(radians(" . $lat . "))
                        * cos(radians(vendors.lat))
                        * cos(radians(vendors.long) - radians(" . $long . "))
                        + sin(radians(" .$lat. "))
                        * sin(radians(vendors.lat))) AS distance"))
            ->orderBy("distance")
            ->get();
        return $vendors;

    }

}
