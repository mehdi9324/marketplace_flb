<?php

namespace App\Repositories\Api;

use App\Models\Order;
use App\Models\User;
use App\Models\Vendor;
use App\Repositories\BaseRepository;
use Illuminate\Http\JsonResponse;

class OrderRepository extends BaseRepository
{
    private $fieldSearchable = [];

    /**
     * @inheritDoc
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Order::class;
    }

    public function createOrder($data):JsonResponse
    {

    }

}
