<?php

namespace App\Repositories\Api;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use App\Models\Vendor;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{
    private $fieldSearchable = [];

    /**
     * @inheritDoc
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Category::class;
    }

}
