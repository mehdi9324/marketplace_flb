<?php

namespace App\Repositories\Api;

use App\Models\Product;
use App\Models\User;
use App\Models\Vendor;
use App\Repositories\BaseRepository;
use Illuminate\Http\JsonResponse;

class ProductRepository extends BaseRepository
{
    private $fieldSearchable = [];

    /**
     * @inheritDoc
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Product::class;
    }

    public function show($data)
    {
        return  $this->allQuery()
            ->where('category_id',$data['category_id'])
            ->whereIn('vendor_id',$data['vendor_ids'])
            ->with(['vendor','category'])
            ->paginate(20);
    }
}
