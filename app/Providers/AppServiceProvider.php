<?php

namespace App\Providers;

use App\Services\Admin\AclAdminService;
use App\Services\Admin\AclAdminServiceInterface;
use App\Services\Admin\AdminService;
use App\Services\Admin\AdminServiceInterface;
use App\Services\Api\OrderService;
use App\Services\Api\OrderServiceInterface;
use App\Services\Api\OtpService;
use App\Services\Api\OtpServiceInterface;
use App\Services\Api\ProductService;
use App\Services\Api\ProductServiceInterface;
use App\Services\Api\UserService;
use App\Services\Api\UserServiceInterface;
use App\Services\Api\CacheSystemService;
use App\Services\Api\CacheSystemServiceInterface;
use App\Services\Api\VendorService;
use App\Services\Api\VendorServiceInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->injectAdminServices();
        $this->injectApiServices();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    private function injectAdminServices()
    {
        $this->app->singleton(AdminServiceInterface::class, AdminService::class);
        $this->app->singleton(OtpServiceInterface::class, OtpService::class);
        $this->app->singleton(AclAdminServiceInterface::class, AclAdminService::class);
        $this->app->singleton(UserServiceInterface::class, UserService::class);
        $this->app->singleton(CacheSystemServiceInterface::class, CacheSystemService::class);
        $this->app->singleton(VendorServiceInterface::class, VendorService::class);
        $this->app->singleton(ProductServiceInterface::class, ProductService::class);
        $this->app->singleton(OrderServiceInterface::class, OrderService::class);
    }

    private function injectApiServices()
    {
        //
    }
}
